import SwiftUI
import Combine

struct MainView: View {
    
    @ObservedObject
    var reader: ReaderFacade
    
    @State
    private var isScannerPresented = false
    @State
    private var isGalleryPresented = false
    
    var body: some View {
        NavigationView {
            if reader.isInitialized && !reader.isProcessing {
                VStack {
                    HStack(spacing: 120) {
                        Button("Iniciar Escaneo") {
                            isScannerPresented.toggle()
                        }
                        .padding(10)
                        .background(Color.accentColor)
                        .foregroundColor(.white)
                        .cornerRadius(8)
                        .fullScreenCover(isPresented: $isScannerPresented, content: {
                            CameraView(reader: reader)
                        })
                    }
                    NavigationLink("", isActive: $reader.areResultsReady) {
                        ResultsView(reader: reader)
                    }.hidden()
                }
            } else if !reader.isDatabasePrepared {
                Text("Loading ... \(reader.downloadProgress)%...")
            } else if reader.isProcessing {
                ProgressView().progressViewStyle(.circular)
            } else {
                Text("Initializing ...")
            }
        }.navigationViewStyle(.stack)
    }
}
