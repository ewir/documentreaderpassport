import SwiftUI

@main
struct DocumentReaderSwiftUI_sampleApp: App {
    var body: some Scene {
        WindowGroup {
            MainView(reader: ReaderFacade())
        }
    }
}
